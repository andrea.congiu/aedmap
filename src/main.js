import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import {Vector as VectorLayer} from 'ol/layer.js';
import VectorSource from 'ol/source/Vector.js';
import XYZ from 'ol/source/XYZ';
import OSMXML from 'ol/format/OSMXML.js';
import {bbox as bboxStrategy} from 'ol/loadingstrategy.js';
import {transformExtent} from 'ol/proj.js';
import {Icon, Style, Circle as CircleStyle, Fill, Stroke} from 'ol/style.js';
import opening_hours from 'opening_hours';
import Select from 'ol/interaction/Select.js';
import Geolocation from 'ol/Geolocation.js';
import Feature from 'ol/Feature.js';
import Point from 'ol/geom/Point.js';
import {fromLonLat} from 'ol/proj';

/* Base Map */

var tileLayer = new TileLayer({
  source: new XYZ({
    url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
  })
});

/* AEDs */

var vectorSource = new VectorSource({
  format: new OSMXML(),
  loader: function(extent, resolution, projection) {
    var epsg4326Extent = transformExtent(extent, projection, 'EPSG:4326');
    var client = new XMLHttpRequest();
    client.open('POST', 'https://overpass-api.de/api/interpreter');
    client.addEventListener('load', function() {
      var features = new OSMXML().readFeatures(client.responseText, {
        featureProjection: map.getView().getProjection()
      });
      vectorSource.addFeatures(features);
    });
    var query = 'node(' +
        epsg4326Extent[1] + ',' + epsg4326Extent[0] + ',' +
        epsg4326Extent[3] + ',' + epsg4326Extent[2] +
        ')[\'emergency\'=\'defibrillator\'];out;';
    client.send(query);
  },
  strategy: bboxStrategy
});

var vectorLayer = new VectorLayer({
  source: vectorSource,
  style: function (feature) {
    if(feature.values_ && feature.values_.opening_hours) {
      var oh = new opening_hours(feature.values_.opening_hours);
      var is_open = oh.getState(new Date());
      if(is_open) {
        var image = new Icon({
          src: 'img/aed-green-32.png',
        });
      } else {
        var image = new Icon({
          src: 'img/aed-red-32.png',
        });
      }
    } else {
      var image = new Icon({
        src: 'img/aed-yellow-32.png',
      });
    }

    var style = new Style({
      image: image
    });

    return style;
  }
});

/* Global Map */

var map = new Map({
  target: 'map',
  layers: [
    tileLayer,
    vectorLayer
  ],
  view: new View({
    center: [1241528.47, 5889358.37],
    zoom: 13,
    maxZoom: 19,
    minZoom: 12
  })
});

/* Map Interactions */

var select = new Select();
map.addInteraction(select);
select.on('select', function(e) {
  var html = '';
  var features = e.target.getFeatures();
  features.forEach(function(feature) {
    feature.getKeys().forEach(function(key) {
      html += `${key}: `;
      html += feature.get(key);
      html += '<br>';
    });
  });
  var overlay = document.getElementById('overlay');
  overlay.innerHTML = html;
  overlay.style.bottom = html ? '0' : '-1rem';
  overlay.style.opacity = html ? '.8' : '0';
});

/* GeoLocation */

var geolocation = new Geolocation({
  trackingOptions: {
    enableHighAccuracy: true
  },
  projection: map.getView().getProjection()
});

var accuracyFeature = new Feature();
geolocation.on('change:accuracyGeometry', function() {
  accuracyFeature.setGeometry(geolocation.getAccuracyGeometry());
});

var positionFeature = new Feature();
positionFeature.setStyle(new Style({
  image: new CircleStyle({
    radius: 6,
    fill: new Fill({
      color: '#3399CC'
    }),
    stroke: new Stroke({
      color: '#fff',
      width: 2
    })
  })
}));

geolocation.on('change:position', function() {
  var coordinates = geolocation.getPosition();
  positionFeature.setGeometry(coordinates ?
    new Point(coordinates) : null);
});

geolocation.on('change:tracking', function(whatever) {
  if(!whatever.oldValue) {
    setTimeout(function() {
      var coordinates = geolocation.getPosition();
      map.getView().setCenter(coordinates);
    }, 2000);
  }
});

var geolocationLayer = new VectorLayer({
  map: map,
  source: new VectorSource({
    features: [accuracyFeature, positionFeature]
  })
});

document.getElementById('tracking-checkbox').addEventListener('change', function() {
  geolocation.setTracking(this.checked);
  geolocationLayer.setVisible(this.checked);
});

document.forms['search'].addEventListener('submit', async function(e) {
  e.preventDefault();
  var query = e.target.firstElementChild.value;
  var url = 'https://nominatim.openstreetmap.org/search?q={QUERY}&format=json';
  url = url.replace('{QUERY}', query);
  var results = (await (await fetch(url)).json());
  if(results) {
    var newPosition = fromLonLat([parseFloat(results[0].lon), parseFloat(results[0].lat)]);
    map.getView().setCenter(newPosition);
  }
  e.target.firstElementChild.value = '';
  return false;
});
